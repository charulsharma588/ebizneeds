const express = require('express');
const dotenv = require('dotenv');
const app = express();
const cors = require('cors');
const connectDB = require('./config/config');
const route = require('./routes/services');


dotenv.config();


connectDB();

app.use(cors());
app.use("/images",express.static("images/"))
app.use('/api/users', route,)
const port = process.env.PORT || 8000;

app.listen(port,  ()=> {  console.log(`listening on port ${port} and env ${process.env.NODE_ENV}`.bgMagenta.white); });

