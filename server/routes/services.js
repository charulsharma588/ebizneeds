const express = require("express");
const router = express.Router();
const fs = require("fs");
const user = require("../models/dbmodel");
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();
const multer = require("multer");
const diskStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "images");
  },
  filename: (req, file, cb) => {
    const fileName = file.originalname;
    cb(null, fileName);
  },
});

const storage = multer({ storage: diskStorage });

dotenv.config();

router.post("/signup", storage.single("img"), (req, res) => {
  console.log(req.body);
  const { name, email, password, ad1, ad2, street, city, zipcode } = req.body;
  const imgpath =
    "http://localhost:" + process.env.PORT + "/images/" + req.file.originalname;
  const newUser = new user({
    imgpath,
    name,
    email,
    password,
    ad1,
    ad2,
    street,
    city,
    zipcode,
  });
  try {
    newUser.save();
    res.status(200).json({ success: true, message: "register successful" });
  } catch (e) {
    res.status(400).json({ success: true, message: e.message });
  }
});

router.post("/login", jsonParser, async (req, res) => {
  const { email, password } = req.body;
  try {
    const newUser = await user.find({ email }).lean();
    const selecteduser = newUser[0];
    if (selecteduser.password === password) {
      selecteduser["state"] = "pass";
      const token = jwt.sign(newUser[0], process.env.JWT_SECRET_KEY);
      //   console.log(token)
      res.status(200).json({ token: token });
    } else {
      const token = jwt.sign(
        { message: "login falied", state: "fail" },
        process.env.JWT_SECRET_KEY
      );
      res.status(200).json({ token: token });
    }
  } catch (e) {
    console.log(e);
    res.status(404).json({ message: e.message });
  }
});

router.get("/listall", async (req, res) => {
  try {
    const newUser = await user.find().lean();

    const token = jwt.sign({newUser}, process.env.JWT_SECRET_KEY);
    //   console.log(token)
    res.status(200).json({ token: token });
  } catch (e) {
    console.log(e);
    res.status(404).json({ message: e.message });
  }
});
module.exports = router;
