const mongoose = require('mongoose');

const userModel = mongoose.Schema({
    name:{
        type:String,
        require:true
    },
    email:{
        type:String,
        require:true
    },
    password:{
        type:String,
        require:true
    },
    ad1:{
        type:String,
        require:true
    },
    ad2:{
        type:String,
        require:true
    },
    city:{
        type:String,
        require:true
    },
    street:{
        type:String,
        require:true
    },
    zipcode:{
        type:String,
        require:true
    },
    imgpath:{
        type:String,
        require:true
    }

},{timestamps:true})


const dataModel = mongoose.model('userInfo',userModel);

module.exports =dataModel