import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { baseUrl } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class AuthServiceService {
  constructor(private http: HttpClient) {}

  login(data: any): Observable<any> {
    return this.http.post(`${baseUrl}users/login`, data);
  }
  register(data: any): Observable<any> {
    let frmdata = new FormData();
    Object.keys(data).map((ky) => {
      frmdata.append(ky, data[ky]);
    });
    console.log(frmdata);
    return this.http.post(`${baseUrl}users/signup`, frmdata);
  }
  fetchall(): Observable<any> {
    return this.http.get(`${baseUrl}users/listall`);
  }
}
