import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServiceService } from '../auth-service.service';
import { JwtHelperService } from "@auth0/angular-jwt";
const helper = new JwtHelperService();

@Component({
  selector: 'app-profiledata',
  templateUrl: './profiledata.component.html',
  styleUrls: ['./profiledata.component.css']
})
export class ProfiledataComponent implements OnInit {
  userdata:any;
  isProfile=false;
  loaded=false

  constructor(private authService:AuthServiceService,private router:Router) { }
  ngOnInit(): void {
    this.loaded=false
    let data = localStorage.getItem("userdata")
    data = data?JSON.parse(data):{}
    this.authService.login(data).subscribe((result) => {
      const decodedToken = helper.decodeToken(result.token);
      // console.log(decodedToken);
      if(decodedToken.state=="pass"){
        console.log(decodedToken)
        this.userdata= decodedToken
        this.loaded = true
      }
      else{
        alert("Login Failed")
        this.router.navigateByUrl("/login")
      }
      
    });

  }
  logout(){
    this.router.navigateByUrl("/login")
  }
  viewProfile(){
    this.isProfile=!this.isProfile
  }
  

}
