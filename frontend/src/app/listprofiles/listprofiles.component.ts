import { Component, OnInit } from '@angular/core';
import { MatDialogRef} from '@angular/material/dialog';
import { AuthServiceService } from '../auth-service.service';
import { JwtHelperService } from "@auth0/angular-jwt";
const helper = new JwtHelperService();


@Component({
  selector: 'app-listprofiles',
  templateUrl: './listprofiles.component.html',
  styleUrls: ['./listprofiles.component.css']
})
export class ListprofilesComponent implements OnInit {

  constructor(public dialogref: MatDialogRef<ListprofilesComponent>,private auth:AuthServiceService) { }
usersdata:any;
  ngOnInit(): void {
    this.auth.fetchall().subscribe(res=>{
      const decodedToken = helper.decodeToken(res.token);
      
        this.usersdata= decodedToken.newUser
      
      
    })
  }
  close(){
    this.dialogref.close()
  }
}
