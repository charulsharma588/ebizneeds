import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  
} from "@angular/forms";
import {MatDialog} from '@angular/material/dialog';
import { Router } from "@angular/router";
import { AuthServiceService } from "src/app/auth-service.service";
import { ListprofilesComponent } from "src/app/listprofiles/listprofiles.component";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  constructor(
    private authService: AuthServiceService,
    private formBuilder: FormBuilder,
    private router:Router,
    public dialog: MatDialog
  ) {}

  userlogin = true;
  userregister = false;
  imagedata: any;
  checkoutForm = this.formBuilder.group({
    name: "",
    email: "",
    password: "",
    ad1: "",
    ad2: "",
    street: "",
    city: "",
    zipcode: "",
    img: "",
  });

  loginForm = this.formBuilder.group({
    email: "",
    password: "",
  });

  //Buttons clicks functionalities
  user_register() {
    this.userlogin = false;
    this.userregister = true;
  }
  user_login() {
    this.userlogin = true;
    this.userregister = false;
  }
  ngOnInit(): void {}
  imgupload(event: any) {
    const imgfile = event.target.files[0];
    this.checkoutForm.patchValue({ img: imgfile });
    const reader = new FileReader();
    reader.onload = () => {
      this.imagedata = reader.result as string;
    };
    reader.readAsDataURL(imgfile);
    // console.log(this.checkoutForm.value)
  }
  loginData() {
   
      localStorage.setItem("userdata",JSON.stringify(this.loginForm.value))
    this.router.navigateByUrl("/dashboard")
    this.loginForm.reset()
    
  }
  listAll(){

    const dialogRef = this.dialog.open(ListprofilesComponent, { 
      height: '70%',
      width: '60%',
      disableClose:true
      
    });
  
  }
  onSubmit() {
    if (this.checkoutForm.valid) {
      this.authService.register(this.checkoutForm.value).subscribe((result) => {
        if (result.success) {
          console.log("success", result);
          alert("Registration Done!");
          this.checkoutForm.reset();
        } else {
          console.log("error", result);
        }
      });
    }
  }
}
